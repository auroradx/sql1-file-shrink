IF OBJECT_ID('tempDB..#shrinkCommands') IS NOT NULL
			DROP TABLE #shrinkCommands
GO
IF OBJECT_ID('tempDB..#DbInfo') IS NOT NULL
			DROP TABLE #DbInfo
GO

WITH fs
AS
(
    SELECT database_id, TYPE, SIZE * 8.0 / 1024 SIZE
    FROM sys.master_files
)
SELECT 
    name as DataBaseName,
    (SELECT SUM(SIZE) FROM fs WHERE TYPE = 0 AND fs.database_id = db.database_id) DataFileSizeMB,
    (SELECT SUM(SIZE) FROM fs WHERE TYPE = 1 AND fs.database_id = db.database_id) LogFileSizeMB
INTO  #DbInfo
FROM sys.databases db
order by name asc
--select * from  #shrinkCommands order by DataBaseName asc
--select * from  #DbInfo order by DataBaseName asc

SELECT 
    'USE [' 
    + databases.name + N'] ;' 
    + CHAR(13) 
    + CHAR(10) 
	+ 'ALTER DATABASE [' 
    + databases.name + N'] SET RECOVERY SIMPLE ;' 
    + CHAR(13) 
    + CHAR(10)
    + 'DBCC SHRINKFILE (N''' 
    + masterFiles.name 
    + N''' , 0, TRUNCATEONLY) ;' 
    + CHAR(13) 
    + CHAR(10) 
	+ 'ALTER DATABASE [' 
    + databases.name + N'] SET RECOVERY FULL ;' 
    + CHAR(13) 
    + CHAR(10)
	+ CHAR(13) 
    + CHAR(10)	 AS sqlCommand,
	databases.name AS DataBaseName,
	--#DbInfo.DataBaseName as dbname,
    #DbInfo.DataFileSizeMB,
    #DbInfo.LogFileSizeMB
INTO
   #shrinkCommands
FROM 
    [sys].[master_files] masterFiles 
    INNER JOIN [sys].[databases] databases ON masterFiles.database_id = databases.database_id 
	INNER JOIN  #DbInfo as  #DbInfo on  #DbInfo.DataBaseName = databases.name
	
WHERE 
		databases.database_id > 4 
	 -- AND databases.name not  in ('MosesCone','ParagonYork','PWD','RBL','RDXDBA','Restore_Interface','Sales','sqldev','TCPC','Tiger','TigerMain','UIAdmin_Aurora','UroChartResults','WestGAPath_Billing') -- Exclude system DBs
ORDER BY databases.name ASC


DECLARE iterationCursor CURSOR

FOR
    SELECT 
        sqlCommand,DataBaseName,LogFileSizeMB ,DataFileSizeMB  
    FROM 
        #shrinkCommands 
	--WHERE sqlCommand  like '%_log%'
--  WHERE sqlCommand  not like '%_log%'
	--ORDER BY LogFileSizeMB DESC
  ORDER BY DataBaseName ASC

OPEN iterationCursor

DECLARE @sqlStatement varchar(max),@DataBaseName varchar(max),@LogFileSizeMB varchar(max),@DataFileSizeMB   varchar(max)

FETCH NEXT FROM iterationCursor INTO @sqlStatement,@DataBaseName,@LogFileSizeMB,@DataFileSizeMB

WHILE (@@FETCH_STATUS = 0)
BEGIN
   
	   --	EXEC(@sqlStatement)
			print @sqlStatement
		
    FETCH NEXT FROM iterationCursor INTO @sqlStatement,@DataBaseName,@LogFileSizeMB,@DataFileSizeMB
END

-- Clean up
CLOSE iterationCursor
DEALLOCATE iterationCursor

IF OBJECT_ID('tempDB..#shrinkCommands') IS NOT NULL
			DROP TABLE #shrinkCommands
IF OBJECT_ID('tempDB..#DbInfo') IS NOT NULL
			DROP TABLE #DbInfo

